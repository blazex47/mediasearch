#include <iostream>
#include <filesystem>
#include <condition_variable>
#include <thread>
#include <vector>
#include <map>

extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/dict.h>
}

#if 0
#include <Windows.h>
#endif

std::condition_variable cv;
std::mutex cv_m;

namespace count
{
int index = 0;
constexpr int max = 10;
std::map<std::string,std::vector<std::string>> lookuptable; 
}


static int64_t get_bit_rate(AVCodecContext *ctx)
{
    int64_t bit_rate;
    int bits_per_sample;

    switch (ctx->codec_type) {
    case AVMEDIA_TYPE_VIDEO:
    case AVMEDIA_TYPE_DATA:
    case AVMEDIA_TYPE_SUBTITLE:
    case AVMEDIA_TYPE_ATTACHMENT:
        bit_rate = ctx->bit_rate;
        break;
    case AVMEDIA_TYPE_AUDIO:
        bits_per_sample = av_get_bits_per_sample(ctx->codec_id);
        bit_rate = bits_per_sample ? ctx->sample_rate * (int64_t)ctx->channels * bits_per_sample : ctx->bit_rate;
        break;
    default:
        bit_rate = 0;
        break;
    }
    return bit_rate;
}

void thread_function(const std::filesystem::path& directory,const std::string& keyword)
{
    std::vector<std::thread> thread_list;

    std::string dirname(directory.u8string());

    count::lookuptable.emplace(dirname,std::vector<std::string>());

    for(auto& p: std::filesystem::directory_iterator(directory))
    {        
        std::string filename(p.path().u8string());

        if(std::filesystem::is_directory(p) == true)
        {
            thread_list.emplace_back(thread_function,p.path(),keyword);
        }
        else if(filename.find("avi") != std::string::npos ||
            filename.find("mkv") != std::string::npos ||
            filename.find("mp4") != std::string::npos ||
            filename.find("m4v") != std::string::npos)
        {
            std::unique_lock<std::mutex> lk(cv_m);

            cv.wait(lk, []{return count::index < count::max;});
            count::index+=1;

            AVFormatContext *fmt_ctx = nullptr;
            AVDictionaryEntry *tag = nullptr;
            int ret;

            if ((ret = avformat_open_input(&fmt_ctx, filename.c_str(), NULL, NULL)))
            {
                char errorbuf[256];
                av_strerror(ret, errorbuf, 256);
                std::cerr << "avformat_open_input Error!: " << filename << " error: " <<  errorbuf <<  std::endl;
                continue;
            }

            if ((ret = avformat_find_stream_info(fmt_ctx, NULL)) < 0) 
            {
                std::cerr << "Cannot find stream information!" << std::endl;
                continue;
            }

            //while ((tag = av_dict_get(fmt_ctx->metadata, "", tag, AV_DICT_IGNORE_SUFFIX)))
            //{ 
            //    std::cout << tag->key << ", " <<  tag->value << std::endl;
            //}

            bool found = false;

            for (size_t i = 0; i < fmt_ctx->nb_streams; i++) 
            {
                AVStream *stream = fmt_ctx->streams[i];
                AVCodecContext *avctx;

                avctx = avcodec_alloc_context3(NULL);
                if (!avctx)
                {
                    std::cerr << "Fail to allocate avcodec!" << std::endl;
                    continue;
                }

                if ((ret = avcodec_parameters_to_context(avctx, stream->codecpar)) < 0) {
                    std::cerr << "avcodec_parameters_to_context Error!" << std::endl;
                    avcodec_free_context(&avctx);
                    continue;
                }

                const char *codec_type = av_get_media_type_string(avctx->codec_type);
                const char *codec_name = avcodec_get_name(avctx->codec_id);
                const char *profile = avcodec_profile_name(avctx->codec_id, avctx->profile);

                if(std::string(codec_name) == keyword)
                {
                    found = true;
                }

                //if(!found) continue;

                //std::cout << "codec_type: " << codec_type << std::endl; 
                //std::cout << "codec_name: " << codec_name << std::endl;
                if (profile)
                {
                    //std::cout << "profile: " << profile << std::endl;
                }

                //if (avctx->codec_tag)
                //{
                //    std::cout << "avctx->codec_tag: " << av_fourcc2str(avctx->codec_tag) << std::endl;                    
                //}

                if(avctx->width)
                {
                    std::cout << "resolution: " << avctx->width << "x"<< avctx->height << std::endl;
                }

                int64_t bitrate = get_bit_rate(avctx);
                if (bitrate != 0) 
                {
                    //std::cout << "bitrate: " << bitrate / 1000 << " kb/s" << std::endl;
                } 
                else if (avctx->rc_max_rate > 0) 
                {
                    //std::cout << "bitrate: max. " << avctx->rc_max_rate  / 1000 << " kb/s" << std::endl;
                }

                avcodec_free_context(&avctx);

                //std::cout << "-----" << std::endl;         
            }

            if(found)
            {
                //std::cout << "filename: " << filename << std::endl;
                    
                //std::cout << "=======" << std::endl;

                count::lookuptable[dirname].push_back(filename);
            }

            //av_dump_format(fmt_ctx, 0, filename.c_str(), 0);

            avformat_close_input(&fmt_ctx);

            count::index-=1;
            cv.notify_all();
        }
    }

    for(auto& elem: thread_list)
    {
        elem.join();
    }
}

#if 0
// Convert a wide Unicode string to an UTF8 string
std::string utf8_encode(const std::wstring &wstr)
{
    if( wstr.empty() ) return std::string();
    int size_needed = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), NULL, 0, NULL, NULL);
    std::string strTo( size_needed, 0 );
    WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), &strTo[0], size_needed, NULL, NULL);
    return strTo;
}

int wmain(int argc, wchar_t  ** argv)
{
    std::string path(utf8_encode(argv[1]));
    std::string keyword(utf8_encode(argv[2]));

    // Set console code page to UTF-8 so console known how to interpret string data
    SetConsoleOutputCP(CP_UTF8);

    // Enable buffering to prevent VS from chopping up UTF-8 byte sequences
    setvbuf(stdout, nullptr, _IOFBF, 1000);
#else
int main(int argc, char ** argv)
{
    std::string path(argv[1]);
    std::string keyword(argv[2]);

#endif

    if(argc < 3)
    {
        std::cerr << "Insufficient Argument!" << std::endl;
        std::cerr << "./out.a <path> <substr>" << std::endl;
        return 1;
    }

    thread_function(std::filesystem::u8path(path), keyword);

    for(const auto& elem: count::lookuptable)
    {
        std::cout << "=======" << std::endl;

        if(elem.second.empty() == false)
        {
            std::cout << elem.first << std::endl;

            for(const auto& item: elem.second)
            {
                std::cout << "=======" << std::endl;

                std::cout << " - " <<  item << std::endl;
            } 
        }
    }

    std::cout << "=======" << std::endl;

    return 0;
}