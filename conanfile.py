from conans import ConanFile, CMake

class EziConverterConan(ConanFile):
   settings = "os", "compiler", "build_type", "arch"
   generators = "cmake", "gcc", "txt"
   default_options = {}

   def requirements(self):
      self.requires("ffmpeg/4.2.1@bincrafters/stable")

   def imports(self):
      self.copy("*.dll", dst="bin", src="bin") # From bin to bin
      self.copy("*.dylib*", dst="bin", src="lib") # From lib to bin

   def build(self):
      cmake = CMake(self)
      cmake.configure()
      cmake.build()